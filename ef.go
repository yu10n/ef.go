package ef

import (
	"os"
)

type File struct{
	gf *os.File
	os.FileInfo
	Error int
}

func Open(filePath string) *File {
	f := &File{Error:0}
	var err error

	f.gf, err = os.Open(filePath)

	if err == nil {
		f.FileInfo, err = f.gf.Stat()
		if err != nil {
			f.Error = 2
		}
	}else{
		f.Error = 1
	}

	return f
}

func (f *File) Close() {
	f.gf.Close()
}

func (f *File) Read(offset int64, size int64) []byte {
	data := make([]byte, size)
	f.gf.ReadAt(data, offset)
	return data
}

func (f *File) ReadAll() []byte {
	return f.Read(0, f.Size())
}

func (f *File) Item(offset int64) byte {
	return f.Read(offset, 1)[0]
}

func ReadALL(filePath string) []byte {
	f := Open(filePath)
	defer f.Close()
	return f.ReadAll()
}